<?php
namespace blog\BlogBundle\DataFixtures\ORM;

use blog\BlogBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
  /**
      * @var ContainerInterface
      */
     private $container;

     public function setContainer(ContainerInterface $container = null)
     {
         $this->container = $container;
     }

     public function load(ObjectManager $manager)
     {

         $user = new User();
         $user->setUsername('admin');

         // the 'security.password_encoder' service requires Symfony 2.6 or higher
         $encoder = $this->container->get('security.password_encoder');
          $password = $encoder->encodePassword($user, 'admin');
         $user->setPassword($password);

         $manager->persist($user);
         $manager->flush();
     }

     public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}
?>
