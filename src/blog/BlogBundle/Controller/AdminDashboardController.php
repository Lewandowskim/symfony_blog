<?php

namespace blog\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use blog\BlogBundle\Form\PostType;
use blog\BlogBundle\Entity\Post;
use blog\BlogBundle\Form\CategoryType;
use blog\BlogBundle\Entity\Category;

class AdminDashboardController extends Controller
{

  /**
  * @Template
  */
  public function adminAction()
  {
    $repository = $this->getDoctrine()->getRepository('BlogBundle:Post');
    $lastPosts = $repository->findBy(
      array(),
      array('id' => 'DESC'), 4
    );

    $repository = $this->getDoctrine()->getRepository('BlogBundle:Post');
    $mostPopular = $repository->findBy(
      array(),
      array('views' => 'DESC'), 3
    );

    return array(
      'last_posts' => $lastPosts,
      'most_popular' => $mostPopular,
    );
  }


  /**
  * @Template
  */
  public function addPostAction(Request $request)
  {

    $post = new Post();
    $form = $this->createForm(PostType::class, $post);

    $form->handleRequest($request);
    if ($form->isValid() && $form->isSubmitted()) {

      $post->setAuthor($this->get('security.token_storage')->getToken()->getUser());

      $em = $this->getDoctrine()->getManager();
      $em->persist($post);
      $em->flush();
      $this->addFlash(
        'posted',
        'Posted successfully!'
      );
      return $this->redirectToRoute('add_post');
    }
    return array('form' => $form->createView());
  }


  public function deletePostAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $post = $em->getRepository('BlogBundle:Post')->findOneById($id);
    $em->remove($post);
    $em->flush();

    $this->addFlash(
      'deleted',
      'Post deleted!'
    );
    return $this->redirectToRoute('admin');
  }

  /**
  * @Template
  */
  public function editPostAction($id, Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $post = $em->getRepository('BlogBundle:Post')->findOneById($id);

    $form = $this->createForm(PostType::class, $post);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $formData = $form->getData();
      $em->flush();

      $this->addFlash(
        'notice',
        'Post updated!'
      );

      return $this->redirectToRoute('admin');
    }

    return array('form' => $form->createView());
  }

  /**
  * @Template
  */
  public function displayPostsAction()
  {
    $repository = $this->getDoctrine()->getRepository('BlogBundle:Post');
    $posts = $repository->findBy(
      array(),
      array('id' => 'DESC')
    );

    return array('posts' => $posts);

  }
  /**
  * @Template
  */
  public function addCategoryAction(Request $request){

    $category = new Category();
    $form = $this->createForm(CategoryType::class, $category);


    $form->handleRequest($request);
    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($category);
      $em->flush();

      $this->addFlash(
        'new-category',
        'Created category!'
      );

      return $this->redirectToRoute('add_category');

    }
    return array(
      'form' => $form->createView(),
    );
  }
  /**
  * @Template
  */
  public function displayCategoriesAction(){
    $em = $this->getDoctrine()->getManager();
    $categories = $em->getRepository('BlogBundle:Category')->findAll();

    return array(
      'categories' => $categories
    );
  }


  public function deleteCategoryAction($id){
    $em = $this->getDoctrine()->getManager();
    $category = $em->getRepository('BlogBundle:Category')->findOneById($id);

    $em->remove($category);
    $em->flush();

    return $this->redirectToRoute('display_categories');
  }

  /**
  * @Template
  */
  public function editCategoryAction($id, Request $Request)
  {
    $em = $this->getDoctrine()->getManager();
    $category = $em->getRepository('BlogBundle:Category')->findOneById($id);

    $form = $this->createForm(CategoryType::class, $category);
    $form->handleRequest($Request);
    if ($form->isSubmitted() && $form->isValid()) {
      $formData = $form->getData();
      $em->flush();
    }

    return array(
      'form' => $form->createView()
    );
  }
}
