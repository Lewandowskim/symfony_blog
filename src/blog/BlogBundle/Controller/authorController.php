<?php
namespace blog\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AuthorController extends Controller
{
    /**
     * @Template
     */
    public function authorAction($name)
    {
      $repository = $this->getDoctrine()->getRepository('BlogBundle:User');
      $authorId = $repository->findOneByUsername($name)->getId();

      $repository = $this->getDoctrine()->getRepository('BlogBundle:Post');
      $posts = $repository->findByAuthor($authorId);

      return array(
        'posts' => $posts,
        'authorName' => $name

      );
    }
}


?>
