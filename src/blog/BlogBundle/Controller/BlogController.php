<?php

namespace blog\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
class BlogController extends Controller
{
    /**
     * @Template
     */
    public function indexAction()
    {
      $repository = $this->getDoctrine()->getRepository('BlogBundle:Post');
      $posts = $repository->findBy(
        array(),
        array('id' => 'DESC')
      );

      $repository = $this->getDoctrine()->getRepository('BlogBundle:Category');
      $categories = $repository->findAll();
      return array(
        'posts' => $posts,
        'cat' => $categories);
    }

    /**
     * @Template
     */
    public function aboutAction()
    {
        return array();
    }

    /**
     * @Template
     */
    public function contactAction()
    {
        return array();
    }


}
