<?php

namespace blog\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
class CategoryController extends Controller
{
    /**
     * @Template
     */
    public function categoryAction($name)
    {
      $repository = $this->getDoctrine()->getRepository('BlogBundle:Category');
      $categoryId = $repository->findOneByName($name)->getId();

      $repository = $this->getDoctrine()->getRepository('BlogBundle:Post');
      $posts = $repository->findByCategory($categoryId);
      return array(
        'categoryName' => $name,
        'posts' => $posts,
      );
    }

}
