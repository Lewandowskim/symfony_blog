<?php

namespace blog\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PostController extends Controller
{
  /**
   * @Template
   */
  public function postAction($id)
  {
    $repository = $this->getDoctrine()->getRepository('BlogBundle:Post');
    $post = $repository->findOneById($id);

    $em = $this->getDoctrine()->getManager();
    $views = $post->getViews();
    $post->setViews($views + 1);
    $em->persist($post);
    $em->flush();


    return array('post' => $post);
  }

}
